While Thanes may perform health and metric operations directly, in multi-tenant
scenarious where several Thanes share a single compute node, it's desirable
that we aggregate health and metric checks that apply to the node as a whole,
rather than have multiple Thanes performing duplicate work.

For this purpose, Teknon is a Thane intended for per-compute node deployment,
that both performs these aggregate checks, and provides a single point of
service for controlling Thanes on the node, and monitoring system health.